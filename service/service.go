package service

import (
	"encoding/json"
	"fmt"
	"github.com/devfeel/mapper"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
	"ms-users/config"
	"ms-users/handler"
	"ms-users/model"
	"net/http"
	"strconv"
	"time"
)

func GetUsers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	users := make([]model.User, 0)
	usersDto := make([]model.UserDto, 0)
	err := config.DB.Find(&users).Error
	if err != nil {
		json.NewEncoder(w).Encode(usersDto)
		return
	}
	mapper.MapperSlice(&users, &usersDto)
	json.NewEncoder(w).Encode(usersDto)
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	users := &model.User{}
	usersDto := &model.UserDto{}
	id, _ := strconv.ParseInt(params["id"], 10, 64)
	err := config.DB.Where("id = ?", id).First(&users).Error
	if err != nil {
		ex := fmt.Sprintf("user not found %d", id)
		response := handler.ErrorResponse{Code: http.StatusNotFound, Message: ex}
		json.NewEncoder(w).Encode(response)
		return
	}
	mapper.Mapper(users, usersDto)
	json.NewEncoder(w).Encode(usersDto)
}

func CreateUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	users := &model.User{}
	userDto := &model.UserDto{}
	json.NewDecoder(r.Body).Decode(&users)
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(users.Password), bcrypt.DefaultCost)
	users.Password = string(hashedPassword)

	if err := config.DB.Where("name = ? AND last_name = ? "+
		"AND user_name =?", users.Name, users.LastName, users.UserName).First(&users).Error; err == nil {
		ex := "user is exists"
		response := handler.ErrorResponse{Code: http.StatusBadRequest, Message: ex}
		json.NewEncoder(w).Encode(response)
		return
	}
	config.DB.Create(&users)
	mapper.Mapper(users, userDto)
	json.NewEncoder(w).Encode(userDto)
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	users := &model.User{}
	usersDto := &model.UserDto{}
	config.DB.First(&users, params["id"])
	json.NewDecoder(r.Body).Decode(&users)
	config.DB.Save(&users)
	mapper.Mapper(users, usersDto)
	json.NewEncoder(w).Encode(usersDto)
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	users := &model.User{}
	usersDto := &model.UserDto{}
	config.DB.Delete(&users, params["id"])
	mapper.Mapper(users, usersDto)
	json.NewEncoder(w).Encode("The User is Deleted Successfully!")

}

func Login(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	users := new(model.User)
	login := &model.Login{}
	usersDto := &model.UserDto{}
	json.NewDecoder(r.Body).Decode(&login)

	config.DB.Where("user_name = ? OR id = ?", login.Username, users.Id).First(&users)

	if err := bcrypt.CompareHashAndPassword([]byte(users.Password), []byte(login.Password)); err != nil {
		ex := "username or password incorrect"
		response := handler.ErrorResponse{Code: http.StatusUnauthorized, Message: ex}
		json.NewEncoder(w).Encode(response)
		return
	}

	claims := &model.JwtCustomClaims{
		Username: users.UserName,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	t, err := token.SignedString([]byte("mySecureSecretKey"))

	if err != nil {
		json.NewEncoder(w).Encode(usersDto)
	}

	response := model.LoginResponseDto{Token: t}

	json.NewEncoder(w).Encode(response)

}
