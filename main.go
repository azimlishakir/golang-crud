package main

import (
	"ms-users/config"
	"ms-users/router"
)

func main()  {
	config.InitialMigration()
	router.InitializeRouter()
}