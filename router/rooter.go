package router

import (
	"github.com/gorilla/mux"
	"log"
	"ms-users/service"
	"net/http"
)

func InitializeRouter() {
	r := mux.NewRouter()

	r.HandleFunc("/users", service.GetUsers).Methods("GET")
	r.HandleFunc("/users/{id}", service.GetUser).Methods("GET")
	r.HandleFunc("/users", service.CreateUser).Methods("POST")
	r.HandleFunc("/users/auth", service.Login).Methods("POST")
	r.HandleFunc("/users/{id}", service.UpdateUser).Methods("PUT")
	r.HandleFunc("/users/{id}", service.DeleteUser).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":9090", r))
}
