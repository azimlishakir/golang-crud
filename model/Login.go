package model

type Login struct {
	Username string `json:"userName"`
	Password string `json:"password"`
}
