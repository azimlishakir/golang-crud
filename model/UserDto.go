package model

type UserDto struct {
	Id int64 `json:"id"`
	Name string `json:"name"`
	LastName string `json:"lastName"`
	Username string `json:"userName"`
	Age int `json:"age"`
}
